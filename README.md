# How it work ?

## To build and publish on NPM

Just add your component .vue in components folder

Then, import your component in wrapper.js and set it globally with Vue.component('nameOfYourComponent', yourComponent)

Then => in webpack.config.js => change the entry in entry: './src/wrapper.js',
And finally uncomment the output part for build plugin and comment the output part for test component

Then => change the version in package.json and finally

```bash```
npm run build
npm publish
```

## To test components

```bash```
npm run dev
```



