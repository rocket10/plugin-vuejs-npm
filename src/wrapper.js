import TextEditInline from './components/TextEditInline.vue';
import SelectEditInline from './components/SelectEditInline.vue';

const MyPlugin = {
    install(Vue, options) {
        Vue.component('text-edit-inline', TextEditInline);
        Vue.component('select-edit-inline', SelectEditInline);
    }
};

if (typeof window !== 'undefined' && window.Vue) {
    window.Vue.use(MyPlugin);
}

export default MyPlugin;