import Vue from 'vue'
import App from './App.vue'
// import TextEditInline from 'vuejs-edit-inline'

// console.log(TextEditInline);

new Vue({
    el: '#app',
    render: h => h(App)
})
